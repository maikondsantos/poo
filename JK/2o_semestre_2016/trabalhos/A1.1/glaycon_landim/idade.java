//Autor: Glaycon Landim da Silveira
//Data: 25-08-2016
//Curso: Programa��o Orientada a Objetos 
//Fun��o: Calcular a idade e retornar se é maior ou menor de idade, e finaliza com o nome do usuario 

package faculdade;
        //usado o pacote javax.swing
    import javax.swing.JOptionPane;
        
public class idade { //criado a classe idade

 
    public static void main(String[]args){
        //criado uma variavel do tipo inteiro (anoAtual), usando a caixa de dialogo com o JoptionPane do tipo String
    int anoAtual = Integer.parseInt(JOptionPane.showInputDialog("Em que ano estamos?")); 
        //criado uma variavel do tipo inteiro (anoNascimento).. 
    int anoNascimento = Integer.parseInt(JOptionPane.showInputDialog("Em que ano voc� nasceu?"));
    int idade = anoAtual-anoNascimento;
    JOptionPane.showMessageDialog(null,"Voce tem: "+idade+ " anos.");
    
    
        //o comando � executado quando a idade for menor que 1, com apenas valores numericos 
        // com testes condicionias if e else
    if (idade < 1){
        System.out.println(JOptionPane.showInputDialog("Idade informada n�o � valida!"));
        //o comando � executado quando a idade � menor que 18 e maior que 1, com apenas valores numericos 
    }else if (idade < 18){
        System.out.println (JOptionPane.showInputDialog("Voc� ainda � menor de idade. Digite seu nome para finalizar"));
    }   //o comando � executado quando a idade � maior que 18
    else if (idade > 18){
    System.out.println(JOptionPane.showInputDialog("Voc� j� � maior de idade. Digite seu nome para finalizar"));
    }   
  }
}
