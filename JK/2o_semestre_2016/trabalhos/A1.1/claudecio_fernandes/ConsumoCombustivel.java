
package consumocombustivel;

import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 Consumo médio de combustível
 * @author claudeciosouza
 */
public class ConsumoCombustivel {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       //declarar variavéis//
         double distancia, litros, consumo;
         
        //Lê a partir da linha de comando//
         Scanner ler = new Scanner(System.in);
         
        //Irar abrir uma caixa de dialogo para inserir a quantidade de combustível abastecido//
        litros = Double.parseDouble(JOptionPane.showInputDialog("Informe a quantidade de combustivel abastecido"));
            
        //Irar abrir uma caixa de dialogo para inserir a distancia percorrida//
        distancia = Double.parseDouble(JOptionPane.showInputDialog("Informe a distancia percorrida"));
        //formula de calculo
        consumo = distancia / litros;
        //Abrir caixa de texto com a resposta do calculo
        JOptionPane.showMessageDialog(null, "Consumo Medio = "+consumo +" km/litros.");
          
                                                
         }
     }